package com.example.alen.places;


import com.google.android.gms.maps.model.LatLng;

public class CustomLocation {

    String title;
    LatLng location;

    public CustomLocation() {}

    public CustomLocation(LatLng location, String title) {
        this.location = location;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }
}
