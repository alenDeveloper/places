package com.example.alen.places;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView mainList;
    static ArrayList<CustomLocation> placesList = new ArrayList<>();
    static ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainList = (ListView) findViewById(R.id.main_list);

        placesList.add(new CustomLocation(new LatLng(0,0), "Add new location.."));


        generateTitleList();
        mainList.setAdapter(adapter);

        mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent mapIntent = new Intent(getApplicationContext(), MapsActivity.class);
                mapIntent.putExtra("placeNumber", i);
                startActivity(mapIntent);

            }
        });
    }

    public void generateTitleList() {
        List<String> strings = new ArrayList<>();

        for(CustomLocation customLocation: placesList) {
            strings.add(customLocation.getTitle());
        }
        adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, strings);
        adapter.notifyDataSetChanged();
    }

    public static void notifyAdapter() {
        List<String> strings = new ArrayList<>();

        for(CustomLocation customLocation: placesList) {
            strings.add(customLocation.getTitle());
        }

        adapter.clear();
        adapter.addAll(strings);
        adapter.notifyDataSetChanged();

    }
}
